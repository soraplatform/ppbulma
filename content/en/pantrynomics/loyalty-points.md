---
title: "What are Loyalty Points?"
image: "/photos/dish.jpg"
description: "Loyalty Points implement the moneyless version of real prices as the Second Law of Value"
tags: ['Points']
date: 2022-07-09
linkb: "/pantrynomics/investment-points"
linkbtext: "investment-points"
linkf: "/pantrynomics/social-points"
linkftext: "Articles"
---


Pantrypoints uses four types of points which represent the four prices and the [four laws of value](https://superphysics.org/social/economics/principles/four-laws-of-value):

1. Social Points -- this addresses [nominal value](https://superphysics.org/social/economics/principles/first-law)

2. Loyalty Points -- this addresses [real value](https://superphysics.org/social/economics/principles/second-law)

3. Trade Points -- this addresses [market value](https://superphysics.org/social/economics/principles/third-law)

4. Investment Points -- this addresses [natural value](https://superphysics.org/social/economics/principles/fourth-law)



## Loyalty Points

Loyalty points represent the excess productivity that is owed by the seller to the buyer for sustaining his livelihood (economic svadharma). 

The repeated purchases of the buyer gives energy back to the seller, through the money or goods/services given to him in exchange.

This energy (as the ability to buy food, electricity, fuel, pay for rent, and supplies, etc) then allows the seller to continue whatever he has dedicated his life towards, whether it be cooking food, selling goods, cutting hair, harvesting crops, etc.

![Cook](/photos/dish.jpg)

If he is passionate with what he has chosen to do, then this energy will increase his productivity and create excess. This excess can then go back to his buyers who supported him with their past purchases. 

The energy given by his loyal customers manifest in our system as loyalty points. 

The energy that leads to excess productivity manifests as free items that those customers can claim using their accumulated points.

The points are served by a web or mobile app that allows users to register loyalty points and claim them. This eliminates the need for loyalty cards or any monetary rewards in line with a moneyless system. 


![Rewards](/screens/rewards/rewards.jpg)


We even allow payment for the service in kind, through our [trade points system](/home), to prove that a totally moneyless system is possible*. This then would save businesses and economies from inflation, financial crises, and debt crises which we have been [warning about since 2015](/social/supersociology/precrisis-years).


> *The [Inca empire](https://gizmodo.com/the-greatest-mystery-of-the-inca-empire-was-its-strange-5872764) proved that an entire country can be sustained by a moneyless system
