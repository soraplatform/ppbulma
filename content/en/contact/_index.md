---
title: "Count me in!"
subtitle: "We're building a totally new online economic system. Feel free to leave an inquiry"
# meta description
description: "Join Pantry!"
title2: "Our Network"
subtitle2: "as of July 2022"
item:
  - image: "/icons/foodrescueph.png"
    link: "https://foodrescueph.site"
  - image: "/icons/megha.png"
    link: "https://www.meghaproducts.com/"  
  - image: "/icons/elivirs.png"
    link: "https://elivirs.com"  
  - image: "/icons/scanner.jpg"
    # link: "https://foodrescueph.site"  
  - image: "/icons/matayog.png"
    link: "https://matayog.com"  
  - image: "/icons/fa.png"
    link: "https://foodforall.xyz"  
  - image: "/icons/neo.png"
    link: "https://www.facebook.com/neoholisticyoga/"  
  - image: "/icons/maharlika.png"
    # link: "#"  
  - image: "/icons/capri.png"    
    link: "https://capricafe.shop"  
  - image: "/icons/angels.png"    
    link: "https://angelshelter.shop"  
  - image: "/icons/ooh.jpg"
    link: "https://www.facebook.com/oilofhope"



title3: "Our Supporters"
subtitle3: "Before the Pandemic (as SORA or Social Resource Allocation)"
support:
  - image: "/icons/wv.jpg"
    link: "https://www.worldvision.org.ph"
  - image: "/logos/activate.png"
    link: "https://aws.amazon.com/activate"  
  - image: "/logos/itp.png"
    link: "http://203.128.241.219"  
  - image: "/logos/magic.png"  
    link: "https://www.mymagic.my"  
  - image: "/logos/vsv.png"      
    link: "https://www.siliconvalley.com.vn"  
---


<!-- <div class="subtitle is-6 has-text-centered mt-5">Our Pantry Network as of June 2022</div>

<img src="/graphics/network.jpg" class="shadow-lg" alt="Pantry Network">


<div class="subtitle is-6 has-text-centered mt-5">Our Past Supporters</div>	

<img src="/graphics/support.jpg" class="shadow-lg" alt="Support"> -->


<!-- - inputs from academics, policymakers, institutions
- translations from people who can speak a language other than English
- donors who want to donate in cash or kind to seed the points system
- interested people who want to try our prototypes and spread the word -->


<!-- <p class="mt-5">Leave us a message or a comment or send an email to <a class="text-color" href="mailto:hello@pantrypoints.com">hello@pantrypoints.com</a></p> -->


<!-- * **Phone: +88 125 256 452** 
* **Address: 360 Main rd, Rio, Brazil** -->
