---
title: "Socratic FAQ"
description: "About Pantrynomics"
image: /og/pantrypoints.jpg
linkf: "/faq"
linkftext: "FAQ"
linkb: "/"
linkbtext: "Home"
---

{{< r ava="asker" >}}
What is Pantrypoints?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
It's a points-based economic system that implements the system proposed by Adam Smith in "The Wealth of Nations".
{{< /l >}}

{{< r ava="asker" >}}
What's wrong with the current economic system?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
It's based on selfish-interest. This leads to marginal utility and 'diminishing returns' which then leads to the concept of profit maximization within Neoclassical Economics.
{{< /l >}}

{{< r ava="asker" >}}
What's wrong with profit maximization?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
Adam Smith says that a society has 4 classes: rent-earners, wage-earners, donation-earners, and profit-earners. 

Profit maximization over-feeds the profit-earners that it starves the other 3 classes. This leads to governments with huge debts, workers needing to work 2 or more jobs, and research not getting funded.

If there is profit-maximization, then there should be tax-maximization, wage-maximization, and donation-maximization. That would be fairer.   
{{< /l >}}

{{< r ava="asker" >}}
How does Pantrypoints reduce profit maximization naturally?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
By using bilateral points instead of money. 

Unlike currency which is multilateral and can be amassed quickly, points are bilateral and takes time to build up. This prevents leveraging, and [emphasizes primary arbitrage over secondary arbitrage](https://superphysics.org/social/economics/principles/arbitrage). 

Modern economics goes for the latter arbitrage as 'buy low, sell high'. The problem is that 'buy low, sell high' is exactly what causes crashes and originates from selfish-interest.  
{{< /l >}}

{{< r ava="asker" >}}
What is your alternative to selfish-interest?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
Common interest. 

It means people think about society more and give importance to the society instead of just caring for the self.  
{{< /l >}}

{{< r ava="asker" >}}
How does Pantrypoints push common interest over selfish-interest?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
Through the bilateral points system.

The bilateral nature creates a chain of transactions that traces the supply chain. This chain is what the Physiocrats called [the Economic Table](https://superphysics.org/social/economics/solutions/modern-economic-table). It will give real-time information to policymakers on the flow on an economy, as well as the blockages that stifle the circulation of value. 
{{< /l >}}

{{< r ava="asker" >}}
Why can't this be done with the current money-system?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
When you buy something with money, the transaction might not be recorded because the seller just wants to get your money and not provide any information about the transaction. This is why tax evasion and avoidance are very common in money-systems. 

Even commercial receipts just list the seller and the item sold. They have no information on the buyer and how he got the money to pay for the item.  
{{< /l >}}


{{< r ava="asker" >}}
So Pantrypoints has its own evasion-resistant taxation system?
{{< /r >}}


{{< l ava="Pantrypoints" >}}
Yes, we call it [Pointtax](/pointtax) where people pay with their productivity at their own pace. This reduces budget deficits while making taxation easier, aligning with [Adam Smith's taxation maxims](https://superphysics.org/social/economics/principles/taxation-maxims). 
{{< /l >}}



{{< r ava="asker" >}}
So Pantrypoints has a transaction system and taxation system rolled into one platform?
{{< /r >}}


{{< l ava="Pantrypoints" >}}
Yes. 

We are prototyping implementations that even have basic Sales, Ordering, Moneyless Point-of-sales (POS), Customer Relationship Management (CRM), Inventory, and Human Resources system. 
{{< /l >}}


{{< r ava="asker" >}}
Is there anything that can't do for businesses?
{{< /r >}}


{{< l ava="Pantrypoints" >}}
It can't do Accounting, Material Requirements Planning (MRP), Product Lifecycle Management (PLM), Logistics, and E-commerce.  

However, it will be able to do world trade, as [Pantry World](/world). This is because global free trade is one of the biggest features of Adam Smith's system. All parts of the system are actually geared towards making free trade possible, with or without money.   
{{< /l >}}


{{< r ava="asker" >}}
How will Pantry World solve problems with the global supply chain when it doesn't do logistics?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
It will solve it indirectly through Pool Clearing.   
{{< /l >}}

{{< r ava="asker" >}}
What's Pool Clearing?
{{< /r >}}


{{< l ava="Pantrypoints" >}}
It's a trade system proposed by [EF Schumacher](https://wikipedia.org/wiki/E._F._Schumacher) wherein the trading countries act as one ['pool'](https://superphysics.org/research/schumacher/pool-clearing/part-1) that coordinates their foreign trade to maximize efficiency.

This is very different from the current world trade system established by Bretton Woods wherein the United States has a superior position in world trade with the US dollar as the international reserve currency. 
{{< /l >}}


{{< r ava="asker" >}}
So Pool Clearing is supposed to end trade wars, right?
{{< /r >}}


{{< l ava="Pantrypoints" >}}
Yes. It's also supposed to harmonize and simplify free trade agreements and make currency crises obsolete. 
{{< /l >}}


{{< r ava="asker" >}}
How do I sign up?
{{< /r >}}


{{< l ava="Pantrypoints" >}}
You can register in [the waitlist](https://hub.pantrypoints.com).  
{{< /l >}}


{{< r ava="asker" >}}
Who maintains these apps?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
We have a volunteers who test and contribute to maintain them. So please don't expect them to be bug-free yet. 
{{< /l >}}

{{< r ava="asker" >}}
When will you have the proper apps done?
{{< /r >}}

{{< l ava="Pantrypoints" >}}
It depends on the customer or stakeholder. We have a few apps that are being used now. But these are for private business and not for the general public.  
{{< /l >}}


<!-- {{< l ava="Pantrypoints" >}}
It's a 

Unlike the current system, it doesn't use money. 
{{< /l >}}

 -->