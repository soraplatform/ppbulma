---
title: "Learn freely"
subtitle: "Pantry Apprentice and Learn use points to pay for learning"
# are social education systems derived from Adam Smith's educational reform proposal in the Wealth of Nations
logo: "/logos/learn.png"
image: "/og/learn.jpg"
description: "Pantry Apprentice and Learn are social education systems derived from Adam Smith's educational reform proposal in the Wealth of Nations"
# youtube: "3FHG_VoDlhs"
# youtube: "DjGsLaUvYcc"
youtube: "_6oLDaX3T00"
applink: https://hub.pantrypoints.com/signup
apptext: "Register in the Waitlist"


feature:
  bannerfeat: /photos/bedcrop.jpg
  title : "Points-based learning and payment"
  # sub: "Pantry will help the unemployed and unbanked get access to food through their local community"
  feature_item:
    - name : "For the Poor"
      #hand holding
      icon : "<svg  viewBox='0 0 640 512' class='icon is-large' fill='dimgray'><path d='M434.7 64h-85.9c-8 0-15.7 3-21.6 8.4l-98.3 90c-.1.1-.2.3-.3.4-16.6 15.6-16.3 40.5-2.1 56 12.7 13.9 39.4 17.6 56.1 2.7.1-.1.3-.1.4-.2l79.9-73.2c6.5-5.9 16.7-5.5 22.6 1 6 6.5 5.5 16.6-1 22.6l-26.1 23.9L504 313.8c2.9 2.4 5.5 5 7.9 7.7V128l-54.6-54.6c-5.9-6-14.1-9.4-22.6-9.4zM544 128.2v223.9c0 17.7 14.3 32 32 32h64V128.2h-96zm48 223.9c-8.8 0-16-7.2-16-16s7.2-16 16-16 16 7.2 16 16-7.2 16-16 16zM0 384h64c17.7 0 32-14.3 32-32V128.2H0V384zm48-63.9c8.8 0 16 7.2 16 16s-7.2 16-16 16-16-7.2-16-16c0-8.9 7.2-16 16-16zm435.9 18.6L334.6 217.5l-30 27.5c-29.7 27.1-75.2 24.5-101.7-4.4-26.9-29.4-24.8-74.9 4.4-101.7L289.1 64h-83.8c-8.5 0-16.6 3.4-22.6 9.4L128 128v223.9h18.3l90.5 81.9c27.4 22.3 67.7 18.1 90-9.3l.2-.2 17.9 15.5c15.9 13 39.4 10.5 52.3-5.4l31.4-38.6 5.4 4.4c13.7 11.1 33.9 9.1 45-4.7l9.5-11.7c11.2-13.8 9.1-33.9-4.6-45.1z'/></svg>"
      content : "Students pay with their work and learn on the job without the debt"
      
    - name : "For Exchangers"
      #question
      icon : "<svg class='icon is-large' fill='dimgray' viewBox='0 0 576 512'><!-- Font Awesome Free 5.15.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License) --><path d='M96,128A64,64,0,1,0,32,64,64,64,0,0,0,96,128Zm0,176.08a44.11,44.11,0,0,1,13.64-32L181.77,204c1.65-1.55,3.77-2.31,5.61-3.57A63.91,63.91,0,0,0,128,160H64A64,64,0,0,0,0,224v96a32,32,0,0,0,32,32V480a32,32,0,0,0,32,32h64a32,32,0,0,0,32-32V383.61l-50.36-47.53A44.08,44.08,0,0,1,96,304.08ZM480,128a64,64,0,1,0-64-64A64,64,0,0,0,480,128Zm32,32H448a63.91,63.91,0,0,0-59.38,40.42c1.84,1.27,4,2,5.62,3.59l72.12,68.06a44.37,44.37,0,0,1,0,64L416,383.62V480a32,32,0,0,0,32,32h64a32,32,0,0,0,32-32V352a32,32,0,0,0,32-32V224A64,64,0,0,0,512,160ZM444.4,295.34l-72.12-68.06A12,12,0,0,0,352,236v36H224V236a12,12,0,0,0-20.28-8.73L131.6,295.34a12.4,12.4,0,0,0,0,17.47l72.12,68.07A12,12,0,0,0,224,372.14V336H352v36.14a12,12,0,0,0,20.28,8.74l72.12-68.07A12.4,12.4,0,0,0,444.4,295.34Z'/></svg>"
      content : "Do Language Exchange based on points"
#      content : "Learn the modules that you want for the job that you want"
      
    - name : "For Companies"
      #users
      icon : "<svg  viewBox='0 0 640 512' class='icon is-large' fill='dimgray'><path d='M96 224c35.3 0 64-28.7 64-64s-28.7-64-64-64-64 28.7-64 64 28.7 64 64 64zm448 0c35.3 0 64-28.7 64-64s-28.7-64-64-64-64 28.7-64 64 28.7 64 64 64zm32 32h-64c-17.6 0-33.5 7.1-45.1 18.6 40.3 22.1 68.9 62 75.1 109.4h66c17.7 0 32-14.3 32-32v-32c0-35.3-28.7-64-64-64zm-256 0c61.9 0 112-50.1 112-112S381.9 32 320 32 208 82.1 208 144s50.1 112 112 112zm76.8 32h-8.3c-20.8 10-43.9 16-68.5 16s-47.6-6-68.5-16h-8.3C179.6 288 128 339.6 128 403.2V432c0 26.5 21.5 48 48 48h288c26.5 0 48-21.5 48-48v-28.8c0-63.6-51.6-115.2-115.2-115.2zm-223.7-13.4C161.5 263.1 145.6 256 128 256H64c-35.3 0-64 28.7-64 64v32c0 17.7 14.3 32 32 32h65.9c6.3-47.4 34.9-87.3 75.2-109.4z'/></svg>"
      content : "Save cash by using points for apprenticeships" 
      # Hire the people with the skills that you need, without paying for those that you don't"


######################### Service #####################


how:
  title : "How it Works for Exchanges"
  steps:
    - id: 1
      image: "/screens/learn1.jpg"
      content: "Browse the list of language exchangers and meetup for a class"
      # content: "List the modules or skill sets that are required by the job that you want, or be an apprentice of the business that you want to join"
    - id: 2
      image: "/screens/learn2.jpg"
      content: "Pay in points after class"
      # content: "Enroll in the modules or complete the requirements of the apprenticeship"
    - id: 3
      image: "/screens/learn3.jpg"
      content: "The teacher can then claim a lesson from you based on his points balance "
      # content: "Take the test in your desired company to get hired, or get hired by your trainer"



    # content : "<ol><li></li><li></li><li></li><li>Pay off your learning with your work</li></ol>"
    # quote: "Most of the improvements made in several branches of philosophy were not made in universities.. For a long time, several of those universities chose to remain the sanctuaries for exploded systems and obsolete prejudices after they had been hunted out of other parts of the world.. In general, the richest and best endowed universities were the slowest in adopting those improvements."
    # quota: "/avatars/smith.png"
    # button:
    #   enable : false
    #   label : "See active cities"
    #   link : "#"

segment:
  articles:
    - title : "Integrates with ISAIAH Match"
      images:
      - "/og/match.jpg"
      content : "Pantry Learn comes with our personality tool called ISAIAH Match to match students with the proper teachers or lessons."
      # quote: "A young man’s education would be more effective, less tedious and expensive if he began working diligently as a journeyman. He would be paid in proportion to the little work he could do.<br> <cite>- Adam Smith</cite>"
      # quota: "/avatars/smith.png"
    # - title : "Pantry Apprentice for 'Blue-Collar' Learning"
    #   images:
    #   - "/photos/planter.jpg"
    #   content : "Pantry Apprentice lets trainees choose their teachers and pay with their own work as apprentices or the future value of their wages through income sharing agreements."
    #   quote: "A young man’s education would be more effective, less tedious and expensive if he began working diligently as a journeyman. He would be paid in proportion to the little work he could do.<br> <cite>- Adam Smith</cite>"
    #   quota: "/avatars/smith.png"         
      # button:
      #  enable : true
      #  label : "What's Basic Universal Revenue?"
      #  link : "https://superphysics.org/social/economicswhat-is-basic-universal-revenue/"

    # - title : "Pantry Learn for 'White-Collar' Learning"
    #   images:
    #   - "https://sorasystem.sirv.com/cards/groups.png"
    #   content : "Education in modules for customizing your skill set. Pantry Learn splits learning into modules which end in a written or practical test or proof of learning, and can be mixed and matched with other modules from other colleges to create custom skill sets that are natural to each person."
    #   quote: "In general, the discipline of colleges and universities is contrived for the interest or ease of the masters, not for the benefit of the students.<br> <cite>- Adam Smith</cite>"
    #   quota: "/avatars/smith.png"         
      #button:
      #  enable : true
      #  label : "What's Basic Universal Revenue?"
      #  link : "https://superphysics.org/social/economicswhat-is-basic-universal-revenue/" 


req:
  title: Based on the requirements in Book 5, Chapter 1 of the Wealth of Nations
  link: "https://superphysics.org/research/smith/wealth-of-nations/book-5/chapter-1/part-3zd"
  btext: "Read the requirements"
  avatar: "/avatars/smith.png"


feedback:
  title: Awards
  item:
    - user : World Vision Philippines
      image: https://sorasystem.sirv.com/logos/wv.jpg
      content: Social Impact Challenge Manila 2019
      # link: "https://worldvision.org.ph/news/social-innovation"

##################### Call to action #####################

call_to_action:
  title : "Register in the Waitlist"
  link1text: "Sure"
  link1 : "https://hub.pantrypoints.com/signup"
  link2text: "I'm not convinced yet"
  link2 : "https://superphysics.org/social/supersociology/solutions/modular-learning/"
  
---
