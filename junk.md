## PUBLIC

CLoudflare pages
no *.domain.com
HUGO_VERSION 0.88.1

7 dimensions, 5 layers, 4 thermodynamics, 3 states, 2 forces, 1 observer (yourself)

                <a href="{{.Permalink}}" title="{{ i18n "readmore" }} - {{ .Title }}" class="button is-primary" style="position: absolute; bottom: 15px !important; margin: none !important;">{{ i18n "readmore" }}</a>

<!-- checking blog -->
<!-- {{ if or (or (eq .Section "post") (eq .Section "blog")) (or (eq .Section "categories") (eq .Section "tags") )}} -->

platform:
  # bannerfeat: "https://res.cloudinary.com/nara/image/upload/v1567936990/photos/incacroplowres.jpg" 
  title : "Our Skillset.."
  # subtitle: "The Inca created a moneyless system that was able to sustain their empire"
  item:
    - name : "Backend: Python Flask, Ruby on Rails, Elixir Phoenix, SQL"
      image : "<svg  viewBox='0 0 288 512' class='icon is-large' fill='dimgray'><path d='M209.2 233.4l-108-31.6C88.7 198.2 80 186.5 80 173.5c0-16.3 13.2-29.5 29.5-29.5h66.3c12.2 0 24.2 3.7 34.2 10.5 6.1 4.1 14.3 3.1 19.5-2l34.8-34c7.1-6.9 6.1-18.4-1.8-24.5C238 74.8 207.4 64.1 176 64V16c0-8.8-7.2-16-16-16h-32c-8.8 0-16 7.2-16 16v48h-2.5C45.8 64-5.4 118.7.5 183.6c4.2 46.1 39.4 83.6 83.8 96.6l102.5 30c12.5 3.7 21.2 15.3 21.2 28.3 0 16.3-13.2 29.5-29.5 29.5h-66.3C100 368 88 364.3 78 357.5c-6.1-4.1-14.3-3.1-19.5 2l-34.8 34c-7.1 6.9-6.1 18.4 1.8 24.5 24.5 19.2 55.1 29.9 86.5 30v48c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16v-48.2c46.6-.9 90.3-28.6 105.7-72.7 21.5-61.6-14.6-124.8-72.5-141.7z'/></svg>"
      content : "Points Banking allows economies to work even after a total financial collapse"

    - name : "Content and On-Page SEO"
      icon : "<svg  viewBox='0 0 480 512' class='icon is-large' fill='dimgray'><path d='M471.99 334.43L336.06 256l135.93-78.43c7.66-4.42 10.28-14.2 5.86-21.86l-32.02-55.43c-4.42-7.65-14.21-10.28-21.87-5.86l-135.93 78.43V16c0-8.84-7.17-16-16.01-16h-64.04c-8.84 0-16.01 7.16-16.01 16v156.86L56.04 94.43c-7.66-4.42-17.45-1.79-21.87 5.86L2.15 155.71c-4.42 7.65-1.8 17.44 5.86 21.86L143.94 256 8.01 334.43c-7.66 4.42-10.28 14.21-5.86 21.86l32.02 55.43c4.42 7.65 14.21 10.27 21.87 5.86l135.93-78.43V496c0 8.84 7.17 16 16.01 16h64.04c8.84 0 16.01-7.16 16.01-16V339.14l135.93 78.43c7.66 4.42 17.45 1.8 21.87-5.86l32.02-55.43c4.42-7.65 1.8-17.43-5.86-21.85z'/></svg>"
      content : "Parties keep their own information preventing info asymmetry"
      
    - name : "Research: Qualitative and Quantitative"
      icon : "<svg  viewBox='0 0 640 512' class='icon is-large' fill='dimgray'><path d='M368 32h-96c-17.67 0-32 14.33-32 32v96c0 17.67 14.33 32 32 32h96c17.67 0 32-14.33 32-32V64c0-17.67-14.33-32-32-32zM208 88h-84.75C113.75 64.56 90.84 48 64 48 28.66 48 0 76.65 0 112s28.66 64 64 64c26.84 0 49.75-16.56 59.25-40h79.73c-55.37 32.52-95.86 87.32-109.54 152h49.4c11.3-41.61 36.77-77.21 71.04-101.56-3.7-8.08-5.88-16.99-5.88-26.44V88zm-48 232H64c-17.67 0-32 14.33-32 32v96c0 17.67 14.33 32 32 32h96c17.67 0 32-14.33 32-32v-96c0-17.67-14.33-32-32-32zM576 48c-26.84 0-49.75 16.56-59.25 40H432v72c0 9.45-2.19 18.36-5.88 26.44 34.27 24.35 59.74 59.95 71.04 101.56h49.4c-13.68-64.68-54.17-119.48-109.54-152h79.73c9.5 23.44 32.41 40 59.25 40 35.34 0 64-28.65 64-64s-28.66-64-64-64zm0 272h-96c-17.67 0-32 14.33-32 32v96c0 17.67 14.33 32 32 32h96c17.67 0 32-14.33 32-32v-96c0-17.67-14.33-32-32-32z'/></svg>"
      content : "Points Banking does not use blockchain and can work offline, just as the Inca were offline"

    - name : "Mobile: Flutter (Android)"
      icon : "<svg  viewBox='0 0 640 512' class='icon is-large' fill='dimgray'><path d='M537.6 226.6c4.1-10.7 6.4-22.4 6.4-34.6 0-53-43-96-96-96-19.7 0-38.1 6-53.3 16.2C367 64.2 315.3 32 256 32c-88.4 0-160 71.6-160 160 0 2.7.1 5.4.2 8.1C40.2 219.8 0 273.2 0 336c0 79.5 64.5 144 144 144h368c70.7 0 128-57.3 128-128 0-61.9-44-113.6-102.4-125.4zM393.4 288H328v112c0 8.8-7.2 16-16 16h-48c-8.8 0-16-7.2-16-16V288h-65.4c-14.3 0-21.4-17.2-11.3-27.3l105.4-105.4c6.2-6.2 16.4-6.2 22.6 0l105.4 105.4c10.1 10.1 2.9 27.3-11.3 27.3z'/></svg>"
      content : "Points Banking can be deployed on the cloud to reduce costs"

    - name : "DevOps: AWS, Google CLoud Platform, Heroku, Gigalixir, Digital Ocean, Vultr"
      icon : "<svg  viewBox='0 0 640 512' class='icon is-large' fill='dimgray'><path d='M256 336h-.02c0-16.18 1.34-8.73-85.05-181.51-17.65-35.29-68.19-35.36-85.87 0C-2.06 328.75.02 320.33.02 336H0c0 44.18 57.31 80 128 80s128-35.82 128-80zM128 176l72 144H56l72-144zm511.98 160c0-16.18 1.34-8.73-85.05-181.51-17.65-35.29-68.19-35.36-85.87 0-87.12 174.26-85.04 165.84-85.04 181.51H384c0 44.18 57.31 80 128 80s128-35.82 128-80h-.02zM440 320l72-144 72 144H440zm88 128H352V153.25c23.51-10.29 41.16-31.48 46.39-57.25H528c8.84 0 16-7.16 16-16V48c0-8.84-7.16-16-16-16H383.64C369.04 12.68 346.09 0 320 0s-49.04 12.68-63.64 32H112c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h129.61c5.23 25.76 22.87 46.96 46.39 57.25V448H112c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h416c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z'/></svg>"
      content : "Points Banking will integrate with Pointtax to allow payments in kind"

    - name : "Machine Learning: Scikit Learn, Sagemaker, Numerical Elixir"
      icon : "<svg  viewBox='0 0 640 512' class='icon is-large' fill='dimgray'><path d='M256 336h-.02c0-16.18 1.34-8.73-85.05-181.51-17.65-35.29-68.19-35.36-85.87 0C-2.06 328.75.02 320.33.02 336H0c0 44.18 57.31 80 128 80s128-35.82 128-80zM128 176l72 144H56l72-144zm511.98 160c0-16.18 1.34-8.73-85.05-181.51-17.65-35.29-68.19-35.36-85.87 0-87.12 174.26-85.04 165.84-85.04 181.51H384c0 44.18 57.31 80 128 80s128-35.82 128-80h-.02zM440 320l72-144 72 144H440zm88 128H352V153.25c23.51-10.29 41.16-31.48 46.39-57.25H528c8.84 0 16-7.16 16-16V48c0-8.84-7.16-16-16-16H383.64C369.04 12.68 346.09 0 320 0s-49.04 12.68-63.64 32H112c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h129.61c5.23 25.76 22.87 46.96 46.39 57.25V448H112c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h416c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z'/></svg>"
      content : "Points Banking will integrate with Pointtax to allow payments in kind"

    - name : "Business Registration and Management"
      icon : "<svg  viewBox='0 0 640 512' class='icon is-large' fill='dimgray'><path d='M256 336h-.02c0-16.18 1.34-8.73-85.05-181.51-17.65-35.29-68.19-35.36-85.87 0C-2.06 328.75.02 320.33.02 336H0c0 44.18 57.31 80 128 80s128-35.82 128-80zM128 176l72 144H56l72-144zm511.98 160c0-16.18 1.34-8.73-85.05-181.51-17.65-35.29-68.19-35.36-85.87 0-87.12 174.26-85.04 165.84-85.04 181.51H384c0 44.18 57.31 80 128 80s128-35.82 128-80h-.02zM440 320l72-144 72 144H440zm88 128H352V153.25c23.51-10.29 41.16-31.48 46.39-57.25H528c8.84 0 16-7.16 16-16V48c0-8.84-7.16-16-16-16H383.64C369.04 12.68 346.09 0 320 0s-49.04 12.68-63.64 32H112c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h129.61c5.23 25.76 22.87 46.96 46.39 57.25V448H112c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h416c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z'/></svg>"
      content : "Points Banking will integrate with Pointtax to allow payments in kind"

    - name : "Business Development and Marketing"
      icon : "<svg  viewBox='0 0 640 512' class='icon is-large' fill='dimgray'><path d='M256 336h-.02c0-16.18 1.34-8.73-85.05-181.51-17.65-35.29-68.19-35.36-85.87 0C-2.06 328.75.02 320.33.02 336H0c0 44.18 57.31 80 128 80s128-35.82 128-80zM128 176l72 144H56l72-144zm511.98 160c0-16.18 1.34-8.73-85.05-181.51-17.65-35.29-68.19-35.36-85.87 0-87.12 174.26-85.04 165.84-85.04 181.51H384c0 44.18 57.31 80 128 80s128-35.82 128-80h-.02zM440 320l72-144 72 144H440zm88 128H352V153.25c23.51-10.29 41.16-31.48 46.39-57.25H528c8.84 0 16-7.16 16-16V48c0-8.84-7.16-16-16-16H383.64C369.04 12.68 346.09 0 320 0s-49.04 12.68-63.64 32H112c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h129.61c5.23 25.76 22.87 46.96 46.39 57.25V448H112c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h416c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16z'/></svg>"
      content : "Points Banking will integrate with Pointtax to allow payments in kind"
  # = image_tag 'https://sorasystem.sirv.com/photos/quipu.jpg', class: 'img-fluid rounded'



  
enable  : true
galleryImages :
  - image : images/portfolio/item-1.jpg
  - image : images/portfolio/item-2.jpg
  - image : images/portfolio/item-3.jpg
  - image : images/portfolio/item-4.jpg
  - image : images/portfolio/item-5.jpg
  - image : images/portfolio/item-6.jpg

clients.yml
enable        : true
title         : "Who trust our judgment"
client_logos  : 
  ["images/clients/client-logo-one.png", "images/clients/client-logo-two.png", "images/clients/client-logo-three.png", "images/clients/client-logo-four.png", "images/clients/client-logo-five.png", "images/clients/client-logo-six.png", "images/clients/client-logo-seven.png", "images/clients/client-logo-eight.png", "images/clients/client-logo-nine.png", "images/clients/client-logo-ten.png"]


{{ with .Site.Data.clients }}
  {{ if .enable }}
  <section class="site-client">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="section-title">
            <h2>{{ .title }}</h2>
          </div>
          <div class="site-client-wrapper">
            {{ range .client_logos }}
            <div class="site-client-item">
              <img src="{{ . | absURL }}" alt="client-logo">
            </div>
            {{ end }}
          </div>
        </div>
      </div>
    </div>
  </section>
  {{ end }}
{{ end }}


https://cdn.jsdelivr.net/gh/alpinejs/alpine@v3.5.0/dist/alpine.min.js

{{ $styles := slice }}

{{ range site.Params.plugins.css }}
  {{ if findRE "^http" .link }}
    <link crossorigin="anonymous" media="all" rel="stylesheet" href="{{ .link | absURL }}" {{.attributes | safeHTMLAttr}} >
  {{ else }}
    {{ $styles = $styles | append (resources.Get .link) }}
  {{ end }}
{{ end }}

{{ $styles := $styles | append (resources.Get "scss/style.scss" | resources.ExecuteAsTemplate "style.scss" . | toCSS) }}

{{ $styles := $styles | resources.Concat "/css/style.css" | minify  | fingerprint "sha512"}}

<style crossorigin="anonymous" media="all" type="text/css" integrity="{{ $styles.Data.Integrity }}">{{$styles.Content | safeCSS}}</style>



  {{ if hugo.IsProduction }}
  {{ partialCached "style.html" . }}
  {{ else }}
  {{ partial "style.html" . }}
  {{ end }} 


  <section class="section my-5">
    <div class="container has-text-centered my-5">
      <h2 class="title is-2">{{ .title | markdownify }}</h2>

      <div class="" x-data="{ tab: window.location.hash ? window.location.hash.substring(1) : '1' }" id="tab_wrapper">
        <div class="tabs is-toggle is-toggle-rounded is-fullwidth are-small">
          <ul>
            <li><span class="h"><a :class="{ 'is-active': tab === '1' }" @click.prevent="tab = '1'; window.location.hash = '1'" href="#" >Step 1</a></span></li>
            <li><span class="h"><a :class="{ '': tab === '2' }" @click.prevent="tab = '2'; window.location.hash = '2'" href="#" >Step 2</a></span></li>
            <li><span class="h"><a :class="{ '': tab === '3' }" @click.prevent="tab = '3'; window.location.hash = '3'" href="#" >Step 3</a></span></li>            
          </ul>
        </div>
        {{ range .steps }}        
          <div class="columns is-mobile is-multiline" x-show="tab === '{{ .id }}'">
            <div class="column is-3-mobile is-2">
              <div class="card has-text-centered">
                <a href="{{ .link }}">
                  <img src="{{ .image | absURL }}" style="height: 100px; width: 100px;" class="card-image">
                </a>
                <p class="card-footer">{{ .content | markdownify }}</p>
              </div>
            </div>       
          </div>  
        {{ end }}
        </div>  
      </div>
    </div>
  </section>
{{ end }}
